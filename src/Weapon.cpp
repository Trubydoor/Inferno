#include "Weapon.hpp"

Weapon::Weapon(tank::Entity* owner,
               std::initializer_list<std::string> hurt_types,
               std::string image, Stats stats) :
    hurt_types(hurt_types), owner(owner), image(image), stats(stats)
{
    timer.start();
}

void Weapon::fire(tank::Vectorf velocity)
{
    if (timer.getTicks() < 60 / stats.getFireRate()) {
        return;
    }
    auto pos = owner->getPos();
    pos += owner->getGraphic()->getSize()/2;
    auto bullet = owner->getWorld()->makeEntity<Bullet>(hurt_types,
                                                        stats.getDamage(),
                                                        pos,
                                                        velocity);

    bullet->makeGraphic(image);
    timer.start();
}