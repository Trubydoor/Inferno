#include <Tank/System/Game.hpp>
#include "MainWorld.hpp"
#include <Tank/Audio/SoundEffect.hpp>
#include <thread>

int main()
{
    tank::Game::initialize({1024,768});
    tank::Game::makeWorld<MainWorld>();
    tank::Game::run();
    tank::SoundEffect sound {"assets/sounds/not_good.ogg"};
    sound.play();
    while (sound.getStatus() == tank::SoundEffect::Status::Playing){
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
    }

}
