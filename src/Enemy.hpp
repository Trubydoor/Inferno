#ifndef ENEMY_HPP
#define ENEMY_HPP
#include <Tank/System/Entity.hpp>
#include "Character.hpp"
#include <boost/range/algorithm.hpp>
#include <Tank/Audio/SoundEffect.hpp>

class Enemy : public Character
{
    static tank::SoundEffect sound;
public:
    Enemy(int hp, tank::Vectorf pos = {0,0});
    ~Enemy();

    tank::Vectorf getPlayerPos();

    tank::Vectorf toPlayer();
};

#endif // ENEMY_HPP
