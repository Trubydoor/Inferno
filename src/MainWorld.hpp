#ifndef MAIN_WORLD_HPP
#define MAIN_WORLD_HPP

#include <vector>
#include <Tank/System/World.hpp>
#include "Player.hpp"
#include "Room.hpp"

class MainWorld : public tank::World
{
    tank::observing_ptr<Room> currentRoom;
    tank::Vectori currentRoomIndex {0,0};
    std::vector<std::vector<std::unique_ptr<Room>>> rooms;

public:
    enum class Direction {
        North,
        South,
        East,
        West,
    };

    MainWorld();
    void draw() override;
    void update() override;
    void movePlayer();
};
#endif
