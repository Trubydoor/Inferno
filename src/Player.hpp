#ifndef PLAYER_HPP
#define PLAYER_HPP
#include <Tank/System/Entity.hpp>
#include <Tank/Graphics/Image.hpp>
#include "Weapon.hpp"
#include "Character.hpp"

class Player : public Character {
    static tank::Image image;
    Weapon weapon;
    tank::Vectorf velocity;
public:
    Player();
    ~Player();
    void onAdded() override;
    void moveBy(tank::Vectorf displacement) override;
};

#endif /* defined(PLAYER_HPP) */
