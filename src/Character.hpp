#ifndef CHARACTER_HPP
#define CHARACTER_HPP
#include <Tank/System/Entity.hpp>

class Character : public tank::Entity
{
    int health;
public:
    Character(int hp, tank::Vectorf pos = {0,0}) : Entity(pos), health{hp} {
        addType("character");
    }

    virtual void damage(int damage) {health -= damage;}

    int getHealth() {return health;}

    virtual void update() override {
        if (health < 0) {
            if (isType("player")) {
                tank::Game::stop();
                return;
            }
            remove();
        }
    }
};

#endif //CHARACTER_HPP