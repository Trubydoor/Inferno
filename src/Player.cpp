#include "Player.hpp"
#include <Tank/System/Keyboard.hpp>

tank::Image Player::image {"assets/images/player.tga"};

Player::Player() : Character{100}, weapon(this, {"enemy"}, "assets/images/bullet1.tga",
                          Weapon::Stats(100,0.2))
{
    makeGraphic(image);
    auto windowSize = tank::Game::window()->getSize();
    setPos((windowSize - getGraphic()->getSize()) / 2);
    addType("player");
}

void Player::onAdded()
{
    clearConnections();
    
    using kbd = tank::Keyboard;
    using key = tank::Key;

    auto moveLeft = [this](){
        moveBy({-5,0});
    };

    auto moveRight = [this](){
        moveBy({5,0});
    };

    auto moveUp = [this](){
        moveBy({0,-5});
    };

    auto moveDown = [this](){
        moveBy({0,5});
    };

    connect(kbd::KeyDown(key::A), moveLeft);
    connect(kbd::KeyDown(key::D), moveRight);
    connect(kbd::KeyDown(key::W), moveUp);
    connect(kbd::KeyDown(key::S), moveDown);

    auto fireLeft = [this](){
        weapon.fire({-8,0});
    };

    auto fireRight = [this](){
        weapon.fire({8,0});
    };

    auto fireUp = [this](){
        weapon.fire({0,-8});
    };

    auto fireDown = [this](){
        weapon.fire({0,8});
    };

    connect(kbd::KeyDown(key::Left), fireLeft);
    connect(kbd::KeyDown(key::Right), fireRight);
    connect(kbd::KeyDown(key::Up), fireUp);
    connect(kbd::KeyDown(key::Down), fireDown);
}

Player::~Player()
{
    tank::Game::stop();
}

void Player::moveBy(tank::Vectorf displacement)
{
    auto collisions = [this]() {
        return collide({"wall","enemy"}).empty();
	};
    tank::Entity::moveBy(displacement, not collisions);
}
