#ifndef WEAPON_HPP
#define WEAPON_HPP
#include <Tank/System/Entity.hpp>
#include <Tank/Graphics/Image.hpp>
#include <Tank/Utility/Timer.hpp>
#include <string>
#include "Bullet.hpp"

class Weapon {
public:
    class Stats {
        int damage;
        float fire_rate;
    public:
        Stats(int damage, float fire_rate) : damage{damage}, fire_rate{fire_rate}
        {}
        int getDamage() {return damage;}
        float getFireRate() {return fire_rate;}
    };

    std::vector<std::string> hurt_types;
private:
    tank::observing_ptr<tank::Entity> owner;
    tank::Image image;
    tank::Timer timer;
    Stats stats;

public:
    Weapon(tank::Entity* owner, std::initializer_list<std::string> hurt_types,
           std::string image, Stats stats);
    void fire(tank::Vectorf velocity);
};

#endif //WEAPON_HPP