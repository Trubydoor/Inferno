#include "Room.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/range/algorithm.hpp>
#include <vector>
#include "Wall.hpp"
#include "Player.hpp"
#include "Door.hpp"
#include "SimpleMinion.hpp"
#include "MainWorld.hpp"

tank::Image Room::spriteSheet{"assets/images/TileMap.tga"};

Room::Room(const std::string& filename, MainWorld& world): world{world}
{
    makeEntity<SimpleMinion>(tank::Vectorf{100,100});
    makeEntity<SimpleMinion>(tank::Vectorf{700,400});

    loadMap(filename);
}

void Room::makePlayer()
{
    player = makeEntity<Player>();
}

void Room::movePlayer(tank::observing_ptr<Room> room)
{
    moveEntity(room, player);
    room->setPlayer(player);
    player = nullptr;
}

void Room::loadMap(const std::string& filename)
{
    boost::property_tree::ptree maptree;
    boost::property_tree::json_parser::read_json(filename, maptree);

    std::vector<int> data;
    auto cont = maptree.get_child("layers").begin()->second.get_child("data");

    for (auto x : cont) {
        data.push_back(x.second.get<int>(""));
    }

    if (data.size() != 192) {
        throw std::runtime_error("uh, nope");
    }

    //boost::range::copy(data, std::ostream_iterator<int>(std::cout, ","));

    auto vec_iter = data.begin();
    for (auto& x : map) {
        for (auto& y : x) {
            y = *vec_iter;
            ++vec_iter;
        }
    }

    makeTiles();
}

void Room::makeTiles()
{
    for (int i = 0; i < map.size(); ++i) {
        for (int j = 0; j < map[i].size(); ++j) {
            switch (map[i][j]) {
                case 5: makeDoor(j,i); break;
            default: makeTile(j,i,map[i][j]);
            }
        }
    }
}

void Room::makeTile(int i, int j, int type)
{
    if (type >= 56 && type <= 67) {
        makeEntity<Wall>(tank::Vectorf(i*64, j*64),type);
        return;
    }
    auto x = spriteSheet;

    x.setClip({64,64}, static_cast<int>(type) - 1);
    x.setPos({i*64,j*64});
    floor.push_back(x);
}

void Room::makeDoor(int i, int j) {
    makeEntity<Door>(tank::Vectorf(i*64, j*64));
}

void Room::update()
{
    World::update();
    eventHandler.propagate();
    if (not player->onScreen()) {
        world.movePlayer();
    }
}

void Room::draw()
{
    //std::cout << "hello from draw!\n";
    for (auto& x : floor) {
        x.draw();
    }
    World::draw();
}
