#include "Bullet.hpp"
#include "Character.hpp"
#include "Wall.hpp"
#include "Door.hpp"
#include <boost/range/algorithm.hpp>

Bullet::Bullet(std::vector<std::string> hurt_types, int damage,
               tank::Vectorf position, tank::Vectorf velocity) :
    Entity(position),
    hurt_types(hurt_types),
    damage(damage),
    velocity(velocity)
{
}

void Bullet::update()
{
    moveBy(velocity);
    if (offScreen()) {
        remove();
    }

    auto entities = collide(hurt_types);

    if (not entities.empty()) {
        tank::observing_ptr<Character> character =
            static_cast<Character*>(entities.front().get());
        character->damage(damage);
        remove();
    }

    bool play_sound = false;

    if (boost::find(hurt_types, "player") == hurt_types.end()) {
        play_sound = true;
    }

    auto walls = collide({"wall"});
    if (not walls.empty()) {
        if (play_sound) {
            Wall& wall = static_cast<Wall&>(*walls.front());
            wall.play_sound();
        }
        this->remove();
    }

    auto doors = collide({"door"});
    if (not doors.empty()) {
        if (play_sound) {
            Door& door = static_cast<Door&>(*doors.front());
            door.play_sound();
        }
        this->remove();
    }
}
