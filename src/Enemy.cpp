#include "Enemy.hpp"

tank::SoundEffect Enemy::sound {"assets/sounds/horrible.ogg"};

Enemy::Enemy(int hp, tank::Vectorf pos) : Character(hp, pos)
{
    addType("enemy");
}

tank::Vectorf Enemy::getPlayerPos()
{
    auto& ent_list = getWorld()->getEntities();
    auto ent_it = boost::find_if(ent_list, [](const tank::observing_ptr<Entity>& e){
        return e->isType("player");
    });

    if (ent_it == ent_list.end()) {
        return {0,0};
    }

    tank::observing_ptr<tank::Entity> player = *ent_it;
    auto player_pos = player->getPos();

    return player_pos;
}

tank::Vectorf Enemy::toPlayer()
{
    return (getPlayerPos() - getPos()).unit();
}

Enemy::~Enemy()
{
    sound.play();
}
