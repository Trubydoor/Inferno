#ifndef ROOM_HPP
#define ROOM_HPP
#include <Tank/System/World.hpp>
#include <Tank/System/Entity.hpp>
#include <Tank/Graphics/Image.hpp>
#include <array>

class MainWorld;
class Player;

class Room : public tank::World
{
    static tank::Image spriteSheet;
    std::array<std::array<int, 16>,12> map;
    void loadMap(const std::string& filename);
    std::vector<tank::Image> floor;
    void makeTiles();
    void makeWall(int i, int j);
    void makeDoor(int i, int j);
    void makeTile(int i, int j, int type);

    void setPlayer(tank::observing_ptr<Player> player) { this->player = player; }

    tank::observing_ptr<Player> player;

    MainWorld& world;
    
public:
    Room(const std::string& filename, MainWorld& world);
    void makePlayer();
    void update() override;
    void draw() override;
    void movePlayer(tank::observing_ptr<Room> room);
    tank::observing_ptr<Player> getPlayer() {return player;}
};

#endif //ROOM_HPP
