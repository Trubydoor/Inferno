#include "MainWorld.hpp"
#include "SimpleMinion.hpp"
#include "Room.hpp"
#include <Tank/System/EventHandler.hpp>

MainWorld::MainWorld()
{
    //std::vector<std::unique_ptr<Room>> rooms2;
    //rooms2.emplace_back(new Room("assets/maps/Map1.json", *this));
    //rooms2.emplace_back(new Room("assets/maps/Map2.json", *this));
    rooms.resize(2);
    rooms[0].resize(2);
    rooms[1].resize(1);
    rooms[0][0].reset(new Room("assets/maps/Map1.json", *this));
    rooms[1][0].reset(new Room("assets/maps/Map2.json", *this));
    rooms[0][1].reset(new Room("assets/maps/Map3.json", *this));
    currentRoom = rooms[currentRoomIndex.x][currentRoomIndex.y];

    currentRoom->makePlayer();
}

void MainWorld::draw()
{
    currentRoom->draw();
    World::draw();
}

void MainWorld::update()
{
    currentRoom->update();
    World::update();
}

void MainWorld::movePlayer()
{
    auto player = currentRoom->getPlayer();

    auto windowSize = tank::Game::window()->getSize();
    auto playerSize = player->getHitbox();
    if (player->getPos().x < 0) {
        --currentRoomIndex.x;
        player->setPos({1024-64, player->getPos().y});
    } else if (player->getPos().x > windowSize.x - playerSize.w) {
        ++currentRoomIndex.x;
        player->setPos({64, player->getPos().y});
    } else if (player->getPos().y < 0) {
        --currentRoomIndex.y;
        player->setPos({player->getPos().x, 768-64});
    } else {
        ++currentRoomIndex.y;
        player->setPos({player->getPos().x, 64});
    }

    auto& newRoom = rooms[currentRoomIndex.x][currentRoomIndex.y];
    currentRoom->movePlayer(newRoom);
    currentRoom = newRoom;
}
