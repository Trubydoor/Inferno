#ifndef WALL_HPP
#define WALL_HPP
#include <Tank/System/Entity.hpp>
#include <Tank/Graphics/Image.hpp>
#include <Tank/Audio/SoundEffect.hpp>

class Wall : public tank::Entity
{
    static tank::Image image;
    tank::SoundEffect sound {"assets/sounds/hit_wall.ogg"};
public:
    Wall(tank::Vectorf pos, int tile) : tank::Entity(pos)
    {
        auto graph = makeGraphic(image);
        graph->setClip({64,64}, tile - 1);

        setPos(pos);
        setHitbox({0,0,64,64});

        addType("wall");
    }

    void play_sound() {sound.play();}
};

#endif //WALL_HPP
