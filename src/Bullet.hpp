#ifndef BULLET_HPP
#define BULLET_HPP
#include <Tank/System/Entity.hpp>
#include <vector>

class Bullet : public tank::Entity
{
    std::vector<std::string> hurt_types;
    int damage;
    tank::Vectorf velocity;

public:
    Bullet(std::vector<std::string> hurt_types, int damage, tank::Vectorf position, tank::Vectorf velocity);

    void update() override;
};

#endif