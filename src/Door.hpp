#ifndef DOOR_HPP
#define DOOR_HPP
#include <Tank/System/Entity.hpp>
#include <Tank/Graphics/Image.hpp>
#include <Tank/Audio/SoundEffect.hpp>

class Door : public tank::Entity
{
    static tank::Image tileMap;
    tank::SoundEffect sound {"assets/sounds/glasses.ogg"};
public:
    Door(tank::Vectorf pos) : tank::Entity(pos)
    {
        auto graph = makeGraphic(tileMap);
        graph->setClip({64,64}, 4);

        setPos(pos);
        setHitbox({0,0,64,64});
        setLayer(1);
        
        addType("door");
    }

    void play_sound() {sound.play();}
};

#endif //DOOR_HPP
