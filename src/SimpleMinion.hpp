#ifndef TESTMINION_HPP
#define TESTMINION_HPP
#include <Tank/Graphics/Image.hpp>
#include "Enemy.hpp"
#include "Weapon.hpp"

class SimpleMinion : public Enemy
{
    static tank::Image image;
    Weapon weapon;
public:
    SimpleMinion(tank::Vectorf pos = {0,0});
    void update() override;
    void fire();
    void move();
};

#endif // TESTMINION_HPP
