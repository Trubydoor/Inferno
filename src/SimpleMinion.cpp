#include "SimpleMinion.hpp"
#include <iostream>

tank::Image SimpleMinion::image {"assets/images/enemy1.tga"};

SimpleMinion::SimpleMinion(tank::Vectorf pos) : Enemy(100, pos),
    weapon(this, {"player"},"assets/images/bullet2.tga",
       Weapon::Stats(20,0.135))
{
    makeGraphic(image);
    addType("testminion");
}

void SimpleMinion::update()
{
    Enemy::update();
    fire();
    move();
}

void SimpleMinion::fire()
{
    weapon.fire(toPlayer() * 6.5);
}

void SimpleMinion::move()
{
    auto collisions = [this](){
        return collide({"enemy","player","wall"}).empty();
    };
    moveBy(toPlayer() * 3, not collisions);
}
